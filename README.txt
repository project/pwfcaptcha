AntiSpam Protect Web Form Captcha Plug-in for Drupal
supports English, German, French, Spanish, Russian localization.

Customize your CAPTCHA with desired colors and fonts. Chose among many image types. 
Add reload button and audio support. Use our smart CAPTCHA technology.

How to install:

    * Create account at protectwebform.com. To do this, just go to the registration page: signup.
    * Generate your CAPTCHA type with all the desired parameters.
    * Follow the link "install" under the new CAPTCHA.
    * Choose the "Drupal Plugin" section and download the file.
    * Unpack this file to  "drupal/modules/" directory so that all package files will have paths like this one: /drupal/modules/protectwebform/*filename*    
    * Find the plug-in "Protect Web Form" in Administer->Site building->Modules  in the admin area of your drupal installation and click appropriate "Activate" link.
    * Go to the Administer->User Management->Protect Web Form section in the admin area of your drupal installation and enter the Image Url and Verification Url.
   
   ** If you need some localization go to the Administer->Site configuration->Localization->"importing a translation" to load the appropriate module localization file (*.po) which could be found in /drupal/modules/protectwebform/po/ directory.   
   ** All problems with this plug-in are discussed on http://www.protectwebform.com/plugin_drupal     

That's all!
